package com.study.editor.service

import javafx.scene.Node

class IdService {

    private var blockCount = 0
    private var lineCount = 0

    fun generateID(aObject: Node): String {
        if (aObject.typeSelector.equals("FlowInsertableLine")) {
            val newId = "line$lineCount"
            lineCount = lineCount + 1
            return newId
        } else {
            val newId = "object$blockCount"
            blockCount = blockCount + 1
            return newId
        }
    }
}