package com.study.editor.blocks

import com.study.editor.graphics.FlowEllipse
import com.study.editor.graphics.FlowInsertableLine
import com.study.editor.graphics.FlowShape
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.geometry.Point2D
import javafx.scene.Node

class StartEndBlock(var point: Point2D, width: Double, height: Double) : FlowBlock {

    val ellipse1: FlowEllipse
    val ellipse2: FlowEllipse
    val line: FlowInsertableLine

    val list: ObservableList<Node> = FXCollections.observableArrayList()

    init {
        val eventPoint = point
        val rWidth = width / 2
        val rHeight = height / 2
        if (eventPoint.x < rWidth) {
            point = Point2D(rWidth + 4, eventPoint.y)
        }
        if (eventPoint.y < rHeight) {
            point = Point2D(point.x, rHeight + 4)
        }
        ellipse1 = FlowEllipse(point, rWidth, rHeight, "Begin")
        val point2 = Point2D(point.x, point.y + 500)
        ellipse2 = FlowEllipse(point2, rWidth, rHeight, "End")
        line = FlowInsertableLine(ellipse1, ellipse2, list)
        ellipse1.line2 = line
        ellipse2.line1 = line
    }

    override fun getTopShape(): FlowShape = ellipse1

    override fun getBottomShape(): FlowShape = ellipse2

    override fun getInsertableLines(): List<FlowInsertableLine> = listOf(line)

    override fun getItems(): ObservableList<Node> = list

    override fun getShapes(): ObservableList<FlowShape> = FXCollections.observableArrayList()
}