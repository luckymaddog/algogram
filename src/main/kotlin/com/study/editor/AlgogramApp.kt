package com.study.editor

import com.study.editor.controller.SchemeController
import javafx.stage.Stage
import tornadofx.*

class AlgogramApp : App(SchemeController::class) {

    override fun start(stage: Stage) {
        importStylesheet("/style.css")
        stage.isResizable = false
        super.start(stage)
    }
}