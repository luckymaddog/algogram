package com.study.editor.graphics

import com.study.editor.controller.State
import javafx.geometry.Point2D
import javafx.scene.shape.Polygon
import org.w3c.dom.Document
import org.w3c.dom.Element

class FlowParallelogram(val point: Point2D, val rWidth: Double, val rHeight: Double, val label: String) : FlowShapeWText(label) {

    override fun getMaxSimbols(): Int {
        return super.getMaxSimbols() - 5
    }

    init {
        // отрисовка идёт с левого нижнего угла параллелограмма
        translateX = point.x - rWidth / 2
        translateY = point.y - rHeight / 2

        val shift = 30

        val corner = translateX + rWidth // угол воображаемого прямоугольника 160x50 в который вписан параллелограм
        val height = translateY - rHeight
        val parallelogram = Polygon(translateX, translateY, corner - shift, translateY, corner, height, translateX + shift, height)
        parallelogram.fill = State.figureColor
        parallelogram.stroke = State.figureColor
        parallelogram.strokeWidth = 2.0

        setGlowableOnMouseEnter()
        children.addAll(parallelogram, textView)

        textField.setMaxSize(120.0, 0.0) // todo(получение ширины фигуры)
    }

    override fun getXmlData(doc: Document): Element {

        val createElement = doc.createElement("Object")
        createElement.setAttribute("type", this.typeSelector)
        createElement.setAttribute("name", id)
        val w = translateX + rWidth / 2
        createElement.setAttribute("X", w.toString())
        val y = translateY + rHeight / 2
        createElement.setAttribute("Y", y.toString())
        createElement.setAttribute("width", rWidth.toString())
        createElement.setAttribute("height", rHeight.toString())
        createElement.appendChild(doc.createTextNode(label))

        return createElement
    }
}