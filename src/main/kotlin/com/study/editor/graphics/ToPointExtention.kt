package com.study.editor.graphics

import javafx.geometry.Point2D
import javafx.scene.input.MouseEvent
import javafx.scene.shape.Circle

fun Circle.toPoint(): Point2D = Point2D(centerX, centerY)

fun MouseEvent.toPoint(): Point2D = Point2D(x, y)