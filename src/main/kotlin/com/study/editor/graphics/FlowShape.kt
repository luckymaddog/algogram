package com.study.editor.graphics

import com.study.editor.blocks.FlowBlock
import com.study.editor.controller.State
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.event.EventHandler
import javafx.scene.Cursor
import javafx.scene.Node
import javafx.scene.effect.DropShadow
import javafx.scene.layout.StackPane
import javafx.scene.paint.Color
import org.w3c.dom.Document
import org.w3c.dom.Element

abstract class FlowShape : StackPane() {

    private val bord = 4
    private val shapeGlow = DropShadow(16.0, Color.AQUA)

    lateinit var line1: FlowInsertableLine
    open lateinit var line2: FlowInsertableLine

    lateinit var flowBlock: FlowBlock

    var isSelected = false

    init {
        setDragListeners()
        setDeletable()
        cursor = Cursor.HAND
        id = State.generateId(this)
    }

    private fun setDeletable() {
        // todo (либо контекстное меню с выбором удалить либо по кнопке DELETE)
        onContextMenuRequested = EventHandler {
            // у линий есть ссылки на шейпы, получим их
            if (::line1.isInitialized && ::line2.isInitialized) {
                val shape1 = line1.shape1 //то что выше
                val shape2 = line2.shape2 //то что ниже

                val parentList = line1.parentList
                var deleteTrash = false
                var trashToDelete = FXCollections.emptyObservableList<Node>()
                if (this.typeSelector.equals("FlowRhomb")) {
                    deleteTrash = true
                    trashToDelete = collectTrash(flowBlock.getItems())
                }

                val line = FlowInsertableLine(shape1, shape2, parentList) // новая связующая линия с мусором
                // свяжем шейпы одной линией
                shape1.line2 = line
                shape2.line1 = line

                State.workSpace.removeAll(flowBlock.getItems())

                val elements = line1.getElements() + line2.getElements()
                State.workSpace.removeAll(elements)
                parentList.removeAll(elements)
                if (deleteTrash) {
                    parentList.removeAll(trashToDelete)
                    State.workSpace.removeAll(trashToDelete)
                }
                //удалим шейп и удалим связанные шейпы временно
                State.workSpace.removeAll(this, shape1, shape2)
                parentList.remove(this)

                // добавляем линию и связанные шейп поверх линии
                State.workSpace.addAll(line.getElements())
                State.workSpace.addAll(shape1, shape2)
                parentList.addAll(line.getElements())

                //delete trash
                if (deleteTrash) {
                    State.shapeList.removeAll(trashToDelete) //здесь левые линии и те самые элементы в цикле
                }
                State.shapeList.remove(this)

                State.insertLineList.removeAll(line1, line2)
                State.insertLineList.add(line)
            }
        }
    }

    private fun collectTrash(trashToDelete: ObservableList<Node>): ObservableList<Node> {
        val newTrash: ObservableList<Node> = FXCollections.observableArrayList()
        newTrash.addAll(trashToDelete)
        for (node in trashToDelete) {
            if (node.typeSelector.equals("FlowRhomb")) {
                val items = (node as FlowShape).flowBlock.getItems()
                val collectTrash = collectTrash(items)
                newTrash.addAll(collectTrash)
            }
        }
        return newTrash
    }


    fun setGlowableOnMouseEnter() {
        setOnMouseEntered {
            if (!isSelected) {
                effect = shapeGlow
            }
        }
        setOnMouseExited {
            if (!isSelected) {
                effect = null
            }
        }
    }

    fun setDragListeners() {
        val dragDelta = Delta()
        onMousePressed = EventHandler { mouseEvent ->
            dragDelta.x = translateX - mouseEvent.sceneX
            dragDelta.y = translateY - mouseEvent.sceneY
            cursor = Cursor.NONE
            toFront()
        }
        onMouseReleased = EventHandler {
            cursor = Cursor.HAND
        }
        onMouseDragged = EventHandler { mouseEvent ->
            val translateX = mouseEvent.sceneX + dragDelta.x
            val translateY = mouseEvent.sceneY + dragDelta.y

            if (translateX > bord && width < 1600 - bord) {
                this.translateX = translateX
            }

            if (translateY > bord && translateY < 1200 - bord) {
                this.translateY = translateY
            }
        }
    }

    open fun getXmlData(doc: Document): Element {
        return doc.createElement("Object")
    }

    internal inner class Delta {
        var x: Double = 0.0
        var y: Double = 0.0
    }
}